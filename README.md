# Zalmoxis

Zalmoxis is a Windows desktop application developed in C# on the .NET Framework platform. It can be used to extract API information from a DLL assembly targeting .NET Framework or .NET Standard to an XML file.

## How to run the program

1. Open **Zalmoxis.sln** in Visual Studio.
2. On the **Debug** menu, select **Start Debugging** (or press **F5**).

## Extracting information from DLL assemblies

If you have a DLL file compiled from a project targeting the .NET Framework or .NET Standard, you can use the reflector tool to extract information about the assembly's public members (classes, methods, properties, enums) to an XML file.

> Note: Extracting API information from DLLs targeting .NET Core is not supported.

To perform the extraction:

1. On the **File** menu, click **New Task | Extract API Info**.
2. Browse for the .dll assembly file.
3. If the DLL was generated from a Type Library (.tlb) file, select the check box **Strip the "Class" suffix...**. This is useful in case of COM type libraries that were converted to DLL with Visual Studio's Type Library Importer (**tlbimp.exe**) tool.
4. Optionally, if the source code of .NET DLL assembly was authored with comments, select the check box **Doc XML file**. This assumes that you have selected the option **XML Documentation File** in Visual Studio project options on the **Build** tab before building the DLL assembly. If you have such a documentation file, then provide its path in this field in order to extract comments from it.
5. Click **Extract**. The tool provides information about the progress in the Log View on the right side. When extraction is complete, you are prompted to save the XML file.
6. Save the XML file to a target folder of your choice.

## Limitations

The reflector tool is experimental and has a few *known* shortcomings or to do items (for example, only classes are extracted, but not fields and interfaces).
