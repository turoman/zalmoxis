﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace UtilLib
{
    /// <summary>
    /// The ApiDocumentation class represents documentation for an API (regardless of the programming language).
    /// This class makes it possible to extract API documentation automatically from a given DLL file,
    /// and store it as XML. Any instance of this class is serializeable to an XML file. 
    /// Also, XML files generated from ApiDocumentation classes can be deserialized back into an instance of this class.
    /// </summary> 
    [Serializable()]
    [XmlRootAttribute("apidoc", IsNullable = false)]
    public class ApiDocumentation
    {
        #region methods

        /// <summary>
        /// Saves (serializes) this API documentation object to an XML file.
        /// </summary>
        /// <param name="path">The path of the XML file.</param>       
        public void SaveToXML(string path)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(ApiDocumentation));
            TextWriter writer = new StreamWriter(path);
            xmlser.Serialize(writer, this);
            writer.Close();
        }

        /// <summary>
        /// Loads (deserializes) an XML file into a new API documentation object.
        /// </summary>
        /// <param name="path">The path of the XML file.</param>        
        public static ApiDocumentation LoadFromXML(string path)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.XmlResolver = new XmlUrlResolver();
            settings.DtdProcessing = DtdProcessing.Parse;
            XmlSerializer xmlser = new XmlSerializer(typeof(UtilLib.ApiDocumentation));
            using (var xmlReader = XmlReader.Create(path, settings))
            {
                return (ApiDocumentation)xmlser.Deserialize(xmlReader);
            }
        }
        
        /// <summary>
        /// Converts the assembly received as argument to an ApiDocumentation object.
        /// </summary>
        public void LoadFromAssembly(Assembly assem, bool isCOM, string xmlDocPath = "")
        {
            try
            {
                // Set the XML documentation property if applicable
                if (!String.IsNullOrEmpty(xmlDocPath) && File.Exists(xmlDocPath))                
                    this.XmlDoc = XElement.Load(xmlDocPath);

                // Create a new API library
                List<ApiLibrary> apiLibs = new List<ApiLibrary>();
                ApiLibrary apiLib = new ApiLibrary();

                // Generate a name and description for this API library
                apiLib.Name = assem.ManifestModule.ToString();                
                string[] descr = {
                    $"This file has been generated from the following assembly: {assem.FullName}\n",
                    $"Generation date: {DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}"
                };
                ApiDescription apiDesc = new ApiDescription();
                apiDesc.Text = descr;
                apiLib.Description = apiDesc;

                // Read the classes from assembly and add them to the ApiLibrary object
                List<ApiClass> apiClasses = LoadClassesFromAssembly(assem, isCOM);
                apiLib.ApiClassList = apiClasses;

                // Read the Enums from assembly
                List<ApiEnumeration> apiEnums = LoadEnumsFromAssembly(assem);
                // Add them to the library only if they contain at least one element
                if (apiEnums.Count > 0)
                {
                    apiLib.ApiEnumerationList = apiEnums;
                }

                // Add the ApiLibrary object to the ApiDocumentation object
                apiLibs.Add(apiLib);
                this.ApiLibraryList = apiLibs;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a list of ApiClass objects extracted from the assembly supplied as argument.
        /// The classes are sorted alphabetically by name.
        /// </summary>
        private List<ApiClass> LoadClassesFromAssembly(Assembly assem, bool isCOM)
        {
            try
            {
                List<ApiClass> apiClasses = new List<ApiClass>();
                List<Type> assemClasses = new List<Type>();
                foreach (Type t in assem.GetTypes())
                {
                    if (t.IsClass && t.IsPublic && !(IsDelegate(t)) && !(t.IsSealed))
                    {
                        // Declare a new class and read the name
                        ApiClass aCls = new ApiClass();

                        // Get the description of this member from the XML docs file
                        if(this.XmlDoc != null)
                        {
                            ApiDescription clsDescription = new ApiDescription();
                            string docID = "T:" + t.Namespace + "." + t.Name;
                            string[] desc = { GetSummary(docID) };
                            clsDescription.Text = desc;
                            aCls.Description = clsDescription;
                        }                        

                        // If this assembly was generated from a COM .tlb file, all classes will have a "Class" suffix
                        if (isCOM)
                            aCls.Name = t.Name.Substring(0, t.Name.Length - 5); // Strip the "class" suffix
                        else
                            aCls.Name = t.Name; // leave the class name as is                                              

                        // Read the class properties
                        List<ApiProperty> cPts = LoadPropertiesFromAssemblyType(t);
                        if (cPts.Count > 0)
                            aCls.Properties = cPts;

                        // Read the class methods
                        List<ApiMethod> cMethods = LoadMethodsFromAssemblyType(t);
                        if (cMethods.Count > 0)
                            aCls.Methods = cMethods;

                        // Read the class events
                        List<ApiEvent> cEvts = LoadEventsFromAssemblyType(t, assem);
                        if (cEvts.Count > 0)
                            aCls.Events = cEvts;

                        // Add the class to the list of classes
                        apiClasses.Add(aCls);
                        Trace.WriteLine($"Finished extracting class {aCls.Name}");
                    }
                }

                // Order classes alphabetically
                apiClasses = apiClasses.OrderBy(cls => cls.Name).ToList();

                return apiClasses;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a documentation string for the member ID supplied as argument.
        /// For the syntax of docID, see https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/processing-the-xml-file 
        /// </summary>
        /// <returns></returns>
        private string GetSummary(string docID)
        {
            try
            {
                string desc = "";   
                XElement membersNode = this.XmlDoc.Element("members");
                IEnumerable<XElement> memberNode =
                    from el in membersNode.Elements()
                    where (string)el.Attribute("name") == docID
                    select el;

                foreach(XElement member in memberNode)                
                    desc = member.Element("summary").Value;
                
                return desc;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.StackTrace);
                throw;
            }            
        }

        /// <summary>
        /// Returns documentation for the parameter that belongs 
        /// to the method identified by <c>docID</c>.
        /// For the syntax of docID, see https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/processing-the-xml-file 
        /// </summary>
        /// <returns></returns>
        private string GetParamDescriptionFromXmlDoc(string docID, string name)
        {
            try
            {
                string desc = "";
                XElement members = this.XmlDoc.Element("members");
                IEnumerable<XElement> paramNode =
                    from member in members.Elements()
                    where (string)member.Attribute("name") == docID
                        from param in member.Elements()
                            where (string)param.Attribute("name") == name
                        select param;

                foreach (XElement item in paramNode)
                    desc = item.Value;

                return desc;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a list of ApiMethod objects extracted from the type supplied as argument.
        /// </summary>
        private List<ApiMethod> LoadMethodsFromAssemblyType(Type t)
        {
            try
            {
                // Create a new list of ApiMethod objects
                List<ApiMethod> apiMethods = new List<ApiMethod>();

                // Extract the methods from the current type
                MethodInfo[] methods = t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);

                foreach (MethodInfo m in methods)
                {
                    if (!m.IsSpecialName && !(m.Name.StartsWith("add_", StringComparison.Ordinal) || m.Name.StartsWith("remove_", StringComparison.Ordinal)))
                    {                      
                        // Createa a new method
                        ApiMethod aMethod = new ApiMethod();

                        // Read the method name 
                        aMethod.Name = m.Name;
                        // Call a utility function to cater for generic types such as List<T>                        
                        aMethod.Type = GetTypeName(m.ReturnType);

                        // Get the method description from the XML docs file
                        if (this.XmlDoc != null)
                        {
                            ApiDescription dsc = new ApiDescription();
                            string docID = GetMethodDocID(m, t);                            
                            string[] desc = { GetSummary(docID) };                            
                            dsc.Text = desc;
                            aMethod.Description = dsc;
                        }

                        // Read method parameters
                        RetrieveMethodParameters(m, aMethod, t);

                        // Add the method to the list of methods
                        apiMethods.Add(aMethod);
                    }
                }

                // Order methods alphabetically
                apiMethods = apiMethods.OrderBy(m => m.Name).ToList();

                return apiMethods;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private void RetrieveMethodParameters(MethodInfo m, ApiMethod aMethod, Type t)
        {
            try
            {
                ParameterInfo[] pms = m.GetParameters();
                if (pms != null)
                {
                    List<ApiParameter> aParams = new List<ApiParameter>();
                    foreach (ParameterInfo pm in pms)
                    {
                        ApiParameter aParam = new ApiParameter();
                        aParam.Name = pm.Name;
                        aParam.Type = GetTypeName(pm.ParameterType);
                        aParam.isIn = pm.IsIn;
                        aParam.isReturn = pm.IsRetval;
                        aParam.isOut = pm.IsOut;

                        // Get the param description from the XML docs file
                        if (this.XmlDoc != null)
                        {
                            ApiDescription dsc = new ApiDescription();
                            string docID = GetMethodDocID(m, t);
                            string[] desc = { GetParamDescriptionFromXmlDoc(docID, aParam.Name) };
                            // Don't write empty descriptions
                            if (desc[0] != "")
                            {
                                dsc.Text = desc;
                                aParam.Description = dsc;
                            }
                        }

                        aParams.Add(aParam);
                    }

                    // Only assign parameters to the method if their count > 0
                    if (aParams.Count > 0)
                        aMethod.Parameters = aParams;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a docID that can be used to obtain the documentation 
        /// of this method from the XML Documentation Comments file.
        /// </summary>
        /// <param name="m">The <c>MethodInfo</c> object.</param>
        /// <param name="t">The type.</param>        
        /// <returns></returns>
        private string GetMethodDocID (MethodInfo m, Type t)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if (m.GetParameters() != null)
                {
                    sb.Append("(");
                    foreach (ParameterInfo p in m.GetParameters())
                    {
                        sb.Append(p.ParameterType);
                        sb.Append(",");
                    }
                    sb.Remove(sb.Length - 1, 1); // strip the last comma
                    sb.Append(")");
                }
                return "M:" + t.Namespace + "." + t.Name + "." + m.Name + sb.ToString();
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a list of ApiProperty objects extracted from the type supplied as argument.
        /// </summary>
        private List<ApiProperty> LoadPropertiesFromAssemblyType(Type t)
        {
            try
            {
                // Create a new list of ApiProperty objects
                List<ApiProperty> apiProperties = new List<ApiProperty>();

                // Extract the properties from the current type
                PropertyInfo[] pts = t.GetProperties( BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public );
                foreach (PropertyInfo pt in pts)
                {   
                    ApiProperty aProperty = new ApiProperty();
                    aProperty.Name = pt.Name;
                    aProperty.Type = GetTypeName(pt.PropertyType);

                    // Get the description of this member from the XML docs file
                    if (this.XmlDoc != null)
                    {
                        ApiDescription dsc = new ApiDescription();
                        String docID = "P:" + t.Namespace + "." + t.Name + "." + pt.Name;
                        string[] desc = { GetSummary(docID) };
                        dsc.Text = desc;
                        aProperty.Description = dsc;
                    }

                    if (pt.CanRead)
                        aProperty.canRead = true;
                    if (pt.CanWrite)
                        aProperty.canWrite = true;

                    // Some properties have index parameters
                    if (pt.GetIndexParameters().Length > 0)
                    {
                        ParameterInfo[] pms = pt.GetIndexParameters();
                        if (pms != null)
                        {
                            List<ApiParameter> aParams = new List<ApiParameter>();
                            foreach (ParameterInfo pm in pms)
                            {
                                ApiParameter aParam = new ApiParameter();
                                aParam.Name = pm.Name;
                                aParam.Type = GetTypeName(pm.ParameterType);
                                aParam.isIn = pm.IsIn;
                                aParam.isOut = pm.IsOut;
                                aParams.Add(aParam);
                            }

                            // Only assign parameters to the property if their count > 0
                            if (aParams.Count > 0)
                                aProperty.indexParameters = aParams;
                        }
                    }

                    apiProperties.Add(aProperty);

                }

                // Order properties alphabetically by name
                apiProperties = apiProperties.OrderBy(p => p.Name).ToList();

                return apiProperties;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a list of ApiEvent objects extracted from the type supplied as argument.
        /// </summary>
        private List<ApiEvent> LoadEventsFromAssemblyType(Type t, Assembly assem)
        {
            try
            {
                // Create a new list of ApiEvent objects
                List<ApiEvent> apiEvents = new List<ApiEvent>();

                // Extract the events from the current type
                EventInfo[] evs = t.GetEvents();
                if (evs.Length > 0)
                {
                    foreach (EventInfo ev in evs)
                    {                       
                        ApiEvent aEvent = new ApiEvent();
                        aEvent.Name = ev.Name;
                        aEvent.returnType = GetTypeName(ev.AddMethod.ReturnType);

                        // Now get the delegate with the same name from the assembly
                        foreach (Type tp in assem.GetTypes())
                        {
                            if (tp.IsPublic && IsDelegate(tp) && tp.Name == ev.EventHandlerType.Name)
                            {
                                MethodInfo mi = tp.GetMethod("Invoke"); // is there a better way to find this method?
                                if (mi != null)
                                {
                                    ParameterInfo[] pi = mi.GetParameters();
                                    if (pi != null)
                                    {
                                        List<ApiParameter> aEvParams = new List<ApiParameter>();
                                        foreach (ParameterInfo p in pi)
                                        {
                                            ApiParameter aEvParam = new ApiParameter();
                                            aEvParam.Name = p.Name;
                                            aEvParam.Type = GetTypeName(p.ParameterType);
                                            aEvParam.isIn = p.IsIn;
                                            aEvParam.isOut = p.IsOut;
                                            aEvParams.Add(aEvParam);
                                        }

                                        // No need to create an empty <parameters> tag if no parameters exist                                        
                                        if (aEvParams.Count > 0)
                                            aEvent.Parameters = aEvParams;
                                    }
                                }
                            }
                        }

                        apiEvents.Add(aEvent);
                    }
                }

                // Order evenths alphabetically by name
                apiEvents = apiEvents.OrderBy(e => e.Name).ToList();

                return apiEvents;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns a list of ApiEnumeration objects from the assembly supplied as argument
        /// </summary>
        /// <param name="assem"></param>
        /// <returns></returns>
        private List<ApiEnumeration> LoadEnumsFromAssembly(Assembly assem)
        {
            try
            {
                List<ApiEnumeration> apiEnums = new List<ApiEnumeration>();

                List<Type> enums = new List<Type>();
                foreach (Type t in assem.GetTypes())
                {
                    if (t.IsEnum && t.IsPublic)
                    {
                        ApiEnumeration aEnum = new ApiEnumeration();
                        aEnum.Name = t.Name;

                        // Get the description of this member from the XML docs file
                        if (this.XmlDoc != null)
                        {
                            ApiDescription dsc = new ApiDescription();
                            String docID = "T:" + t.Namespace + "." + t.Name;
                            string[] desc = { GetSummary(docID) };
                            dsc.Text = desc;
                            aEnum.Description = dsc;
                        }

                        FieldInfo[] fields = t.GetFields();
                        List<ApiEnumMember> aEnumMembers = new List<ApiEnumMember>();
                        foreach (FieldInfo field in fields)
                        {
                            if (!field.IsSpecialName)
                            {
                                ApiEnumMember aMember = new ApiEnumMember();
                                aMember.Name = field.Name;
                                int numVal = (int)field.GetValue(field);
                                aMember.NumberValue = numVal.ToString();

                                // Get the description of this member from the XML docs file
                                if (this.XmlDoc != null)
                                {
                                    ApiDescription dsc = new ApiDescription();
                                    String docID = "F:" + t.Namespace + "." + t.Name + "." + aMember.Name;
                                    string[] desc = { GetSummary(docID) };
                                    dsc.Text = desc;
                                    aMember.Description = dsc;
                                }

                                aEnumMembers.Add(aMember);
                            }
                        }

                        if (aEnumMembers.Count > 0)
                            aEnum.Members = aEnumMembers;

                        apiEnums.Add(aEnum);
                    }
                }

                // Order enums alphabetically by name
                apiEnums = apiEnums.OrderBy(en => en.Name).ToList();

                return apiEnums;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns true if a type is delegate.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsDelegate(Type type)
        {
            return typeof(Delegate).IsAssignableFrom(type.BaseType);
        }

        /// <summary>
        /// Returns a string which represents the data type of a type.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static string GetTypeName(Type t)
        {
            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(List<>))
                return String.Format("List<{0}>", t.GetGenericArguments()[0].FullName);
            else
                return t.FullName;
        }
                                                            
        /// <summary>
        /// Returns only those methods that are constructor methods.
        /// </summary>
        /// <param name="cls"></param>
        /// <returns></returns>
        private List<ApiMethod> FilterConstructorMethods(ApiClass cls)
        {
            try
            {                
                var filtered = from ct in cls.Methods
                               where ct.isConstructor == true
                               select ct;
                List<ApiMethod> constructMethods = filtered.Cast<ApiMethod>().ToList();
                return constructMethods;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns only those methods that are NOT constructor methods.
        /// </summary>
        /// <param name="cls"></param>
        /// <returns></returns>
        private List<ApiMethod> FilterNonConstructorMethods(ApiClass cls)
        {
            try
            {
                var filtered = from ct in cls.Methods
                               where ct.isConstructor == false
                               select ct;
                List<ApiMethod> methods = filtered.Cast<ApiMethod>().ToList();
                return methods;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }
                    
        #endregion

        #region properties

        /// <summary>
        /// .NET DLLs may have an accompanying XML Documentation Comments file, 
        /// see https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/
        /// This property gets or sets the XML documentation file generated by Visual Studio, if one is available.
        /// </summary>
        XElement XmlDoc { get; set; }

        #endregion

        #region fields

        /// <summary>
        /// Provides the link to the schema.
        /// </summary>
        [XmlAttribute("noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string noNamespace;

        /// <summary>
        /// Contains a list of <c>ApiLibrary</c> objects.
        /// </summary>
        [XmlArrayAttribute("libraries", IsNullable = false)]
        public List<ApiLibrary> ApiLibraryList;

        #endregion
    }

    /// <summary>
    /// A library is a collection of classes, functions, interfaces, enumerations, etc.
    /// </summary>
    [XmlType("library")]
    public class ApiLibrary
    {
        /// <summary>
        /// Represents a library name.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Represents the library version.
        /// </summary>
        [XmlAttribute("version")]
        public string Version;

        /// <summary>
        /// Represents the programming language of this library.
        /// </summary>
        [XmlAttribute("progLang")]
        public string ProgLang;

        /// <summary>
        /// Represents the description of this library.
        /// </summary>
        [XmlElement("description", IsNullable=true, Order = 1)]
        public ApiDescription Description; 
                
        /// <summary>
        /// Represents the list of classes contained by this library.
        /// </summary>
        [XmlArrayAttribute("classes", IsNullable=true, Order = 2)]
        public List<ApiClass> ApiClassList;

        /// <summary>
        /// Represents the list of interfaces contained by this library.
        /// </summary>
        [XmlArrayAttribute("interfaces", IsNullable = true, Order = 3)]
        public List<ApiInterface> ApiInterfaceList;

        /// <summary>
        /// Represents the list of enumerations contained by this library.
        /// </summary> 
        [XmlArrayAttribute("enumerations", IsNullable = true, Order = 4)]
        public List<ApiEnumeration> ApiEnumerationList;
        
        /// <summary>
        /// Represents the list of functions contained by this library.
        /// </summary>
        [XmlArrayAttribute("functions", IsNullable = true, Order = 5)]
        public List<ApiMethod> ApiFunctionList;

    }
    
    /// <summary>
    /// Represents an API class entity.
    /// </summary>
    [XmlType("class")]
    public class ApiClass
    {
        /// <summary>
        /// Represents the name of the API class.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the optional description of the class.
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 1)]
        public ApiDescription Description;

        /// <summary>
        /// Represents the list of properties for this class.
        /// </summary>
        [XmlArray("properties", IsNullable = true, Order = 2)]
        public List<ApiProperty> Properties;

        /// <summary>
        /// Represents the list of methods for this class.
        /// </summary>
        [XmlArray("methods", IsNullable = true, Order = 3)]
        public List<ApiMethod> Methods;

        /// <summary>
        /// Represents the list of events for this class.
        /// </summary>
        [XmlArray("events", IsNullable = true, Order = 4)]
        public List<ApiEvent> Events;

        /// <summary>
        /// Represents the optional example section for the class.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 5)]
        public ApiExample Example;

    }

    /// <summary>
    /// Represents an API interface entity.
    /// </summary>
    [XmlType("interface")]
    public class ApiInterface
    {
        /// <summary>
        /// Represents the name of the API interface.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the optional description of the interface.
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 1)]
        public ApiDescription Description;

        /// <summary>
        /// Represents the list of properties for this interface.
        /// </summary>
        [XmlArray("properties", IsNullable = true, Order = 2)]
        public List<ApiProperty> Properties;

        /// <summary>
        /// Represents the list of methods for this interface.
        /// </summary>
        [XmlArray("methods", IsNullable = true, Order = 3)]
        public List<ApiMethod> Methods;

        /// <summary>
        /// Represents the list of events for this interface.
        /// </summary>
        [XmlArray("events", IsNullable = true, Order = 4)]
        public List<ApiEvent> Events;

        /// <summary>
        /// Represents the optional example section for the interface.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 5)]
        public ApiExample Example;

    }

    /// <summary>
    /// Represents an enumeration entity.
    /// </summary>
    [XmlType("enumeration")]
    public class ApiEnumeration
    {
        /// <summary>
        /// Represents the name of the enumeration.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the description of the enumeration.
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 1)]
        public ApiDescription Description;

        /// <summary>
        /// Represents the list of members for this enumeration.
        /// </summary>
        [XmlArrayAttribute("members", IsNullable = true, Order = 2)]
        public List<ApiEnumMember> Members;

        /// <summary>
        /// Represents the optional example section for this enumeration.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 3)]
        public ApiExample Example;

    }

    /// <summary>
    /// Represents an enumeration member.
    /// </summary>
    [XmlType("member")]
    public class ApiEnumMember
    {
        /// <summary>
        /// Represents the name of the enumeration member.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Represents the numeric value (required in some environments) required to acess this enumeration member.
        /// </summary>
        [XmlAttribute("numberValue")]
        public string NumberValue;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the optional description of the enumeration member.
        /// </summary>
        [XmlElement("description", IsNullable = true)]
        public ApiDescription Description;
    }
    
    /// <summary>
    /// Represents an event (for a class).
    /// </summary>
    [XmlType ("event")]
    public class ApiEvent 
    {
        /// <summary>
        /// Represents the name of the event.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Represents the return type of the event.
        /// </summary>
        [XmlAttribute("returnType")]
        public string returnType;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the optional description of the event. 
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 1)]
        public ApiDescription Description;

        /// <summary>
        /// Represents the list of parameters for this event. 
        /// </summary>
        [XmlArrayAttribute("parameters", IsNullable = true, Order = 2)]
        public List<ApiParameter> Parameters;

        /// <summary>
        /// Represents the optional example section for this event.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 3)]
        public ApiExample Example;

    }
    
    /// <summary>
    /// Represents a desription entity. This description has mixed type in the (de)serialized XML file.
    /// </summary>
    [XmlType]
    public class ApiDescription
    {
        /// <summary>
        /// The actual XML text of this description.
        /// </summary>
        [XmlText()]
        public string[] Text; 
    }

    /// <summary>
    /// Represents an examples entity. This description has mixed type in the (de)serialized XML file.
    /// </summary>
    [XmlType]
    public class ApiExample
    {
        /// <summary>
        /// The actual XML text of this example section.
        /// </summary>
        [XmlTextAttribute()]
        public string[] Text;
    }

    /// <summary>
    /// Represents a single property entity of a class.
    /// </summary>
    [XmlType ("property")]
    public class ApiProperty
    {
        /// <summary>
        /// Represents the name of the property.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Represents the data type (int, string) of the property.
        /// </summary>
        [XmlAttribute("dataType")]
        public string Type;

        /// <summary>
        /// Boolean flag which indicates if the property provides "get" access.
        /// </summary>
        [XmlAttribute("canRead")]
        public bool canRead;

        /// <summary>
        /// Boolean flag which indicates if the property provides "set" access.
        /// </summary>
        [XmlAttribute("canWrite")]
        public bool canWrite;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the list of index parameters applicable to this method.
        /// </summary>
        [XmlArray("indexParameters", IsNullable = true, Order = 2)]
        public List<ApiParameter> indexParameters;

        /// <summary>
        /// Represents the optional description of this property.
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 3)]
        public ApiDescription Description;

        /// <summary>
        /// Represents a list of possible return errors for this property.
        /// </summary>
        [XmlArrayAttribute("returnErrors", IsNullable = true, Order = 4)]
        public List<ApiReturnError> returnErrors;

        /// <summary>
        /// Represents the optional example section for this property.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 5)]
        public ApiExample Example;

    }

    /// <summary>
    /// Represents a single return error.
    /// </summary>
    [XmlType("returnError")]
    public class ApiReturnError
    {
        /// <summary>
        /// Represents the error code of this error.
        /// </summary>
        [XmlAttribute("errorCode")]
        public string errorCode;

        /// <summary>
        /// Represents the description of this error.
        /// </summary>
        [XmlElement("description", IsNullable = true)]
        public ApiDescription Description;
    }

    /// <summary>
    /// Represents either an API class method or a library function.
    /// </summary>
    [XmlType ("method")]
    public class ApiMethod
    {
        /// <summary>
        /// Represents the name of this method.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// Represents the data type returned by this method.
        /// </summary>
        [XmlAttribute("returnType")]
        public string Type;

        /// <summary>
        /// Used to specify if this method is a constructor.
        /// </summary>
        [XmlAttribute("isConstructor")]
        public bool isConstructor;

        /// <summary>
        /// Flags this member as obsolete.
        /// </summary>
        [XmlAttribute("isObsolete", DataType = "boolean")]
        public bool isObsolete;

        /// <summary>
        /// Represents the optional description of this method.
        /// </summary>
        [XmlElement("description", IsNullable = true, Order = 2)]
        public ApiDescription Description;

        /// <summary>
        /// Represents the list of parameters applicable to this method.
        /// </summary>
        [XmlArrayAttribute("parameters", IsNullable = true, Order = 3)]
        public List<ApiParameter> Parameters;

        /// <summary>
        /// Represents a list of return errors applicable to this method.
        /// </summary>
        [XmlArrayAttribute("returnErrors", IsNullable = true, Order = 4)]
        public List<ApiReturnError> returnErrors;

        /// <summary>
        /// Represents the optional example section for this method.
        /// </summary>
        [XmlElement("example", IsNullable = true, Order = 5)]
        public ApiExample Example;

    }

    /// <summary>
    /// Represents a parameter entity.
    /// </summary>
    [XmlType ("parameter")]
    public class ApiParameter
    {
        /// <summary>
        /// The name of the parameter.
        /// </summary>
        [XmlAttribute("name")]
        public string Name;

        /// <summary>
        /// The data type of the parameter.
        /// </summary>
        [XmlAttribute("dataType")]
        public string Type;

        /// <summary>
        /// True if the direction of the parameter is "in".
        /// </summary>
        [XmlAttribute("isIn")]
        public bool isIn;

        /// <summary>
        /// True if the direction of the parameter is "out".
        /// </summary>
        [XmlAttribute("isOut")]
        public bool isOut;

        /// <summary>
        /// True if the direction of the parameter is a return value.
        /// </summary>
        [XmlAttribute("isReturn")]
        public bool isReturn;

        /// <summary>
        /// Specifies the default value of this parameter.
        /// </summary>
        [XmlAttribute("defaultValue")]
        public string defaultValue;

        /// <summary>
        /// The optional description of the parameter.
        /// </summary>
        [XmlElement("description", IsNullable = true)]
        public ApiDescription Description;
    }
    
}
