﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace UtilLib
{
    /// <summary>
    /// In charge of writing traces to the taget (TextBox) control.
    /// </summary>
    public class TextBoxTraceListener : TraceListener
    {        
        /// <summary>
        /// Both these fields get populated when the class instance is invoked.
        /// The first is the target text box control. The second is a variable of type StringSendDelegate.
        /// </summary>
        private TextBox _target;
        private StringSendDelegate _invokeWrite;

        /// <summary>
        /// Declare a delegate type; it points to any method that takes a string as argument and returns void.        
        /// </summary>
        /// <param name="message"></param>
        private delegate void StringSendDelegate(string message);

        /// <summary>
        /// Initializes a new instance of TextBoxTraceListener.         
        /// </summary>
        /// <param name="target"></param>
        public TextBoxTraceListener(TextBox target)
        {
            _target = target;
            // Instantiate the delegate type (i.e. add a target method to the delegate instance).
            _invokeWrite = new StringSendDelegate(SendString);
        }

        /// <summary>
        /// Remember this class extends TraceListener. This method is required to implement the abstract member.
        /// It writes a value (message) to the listener that implements the TraceListener class.
        /// </summary>
        /// <param name="message"></param>
        public override void Write(string message)
        {            
            if (_target.IsHandleCreated)
                // This is where the delegate call occurs                
                _target.Invoke(_invokeWrite, new object[] { message });            
        }

        /// <summary>
        /// Remember this class extends TraceListener. This method is required to implement the abstract member.        
        /// It writes a value (message) to the listener that implements the TraceListener class, 
        /// followed by a line terminator.
        /// </summary>
        /// <param name="message"></param>
        public override void WriteLine(string message)
        {
            if(_target.IsHandleCreated)  
                // This is where the delegate call occurs
                _target.Invoke(_invokeWrite, new object[] { message + Environment.NewLine });
        }               

        /// <summary>
        /// Private method which updates the text of the target textbox, and scrolls down the textbox.
        /// </summary>
        /// <param name="message"></param>
        private void SendString(string message)
        {
            // Append the current message to the target text box
            _target.AppendText(message);            
            // Not calling Application.DoEvents() may result in the following exception while debugging:
            // --Managed Debugging Assistant 'ContextSwitchDeadlock'--
            Application.DoEvents();
        }
    }
}
