﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using UtilLib;

namespace Zalmoxis
{
    /// <summary>
    /// Form for selecting a DLL assembly to reflect.
    /// </summary>
    public partial class FormExtract : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public FormExtract()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This method is required to make it possible to output trace messages to the Log text box of the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormExtractApiInfo_Load(object sender, EventArgs e)
        {
            TextBoxTraceListener _textBoxListener = new TextBoxTraceListener(textBoxViewLog);
            Trace.Listeners.Add(_textBoxListener);
        }

        /// <summary>
        /// When the Browse button is clicked on the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBrowseClick(object sender, EventArgs e)
        {
            try
            {
                // Clear any previous errors				
                errorProvider1.Clear();

                OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.Filter = "DLL files|*.dll";
                openDlg.CheckFileExists = true;
                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    textBoxSourceDll.Text = openDlg.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Performs the actual extraction of API information from the given DLL.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonExtractClick(object sender, EventArgs e)
        {
            try
            {
                // Clear any previous errors				
                errorProvider1.Clear();

                // Clear the log
                textBoxViewLog.Clear();

                // The source DLL file must exist         
                if (!File.Exists(textBoxSourceDll.Text))
                {
                    errorProvider1.SetIconAlignment(textBoxSourceDll, ErrorIconAlignment.MiddleRight);
                    errorProvider1.SetError(textBoxSourceDll, "This path does not exist");
                }                
                // The XML documentation file is optional, but must exist if provided
                else if (!String.IsNullOrWhiteSpace(textBoxDocXml.Text) && !File.Exists(textBoxDocXml.Text))
                {
                    errorProvider1.SetIconAlignment(textBoxDocXml, ErrorIconAlignment.MiddleRight);
                    errorProvider1.SetError(textBoxDocXml, "This path does not exist");
                }
                else
                {
                    // Load the assembly from the DLL file.                    
                    // See also https://msdn.microsoft.com/en-us/library/1009fa28(v=vs.110).aspx                    
                    Assembly assem = Assembly.LoadFrom(textBoxSourceDll.Text);

                    // Convert the assembly into an ApiDocumentation object
                    ApiDocumentation apiDoc = new ApiDocumentation();
                    apiDoc.LoadFromAssembly(assem, checkCOM.Checked, textBoxDocXml.Text);
                    
                    Form outcome = new FormOutcome();
                    outcome.ShowDialog();

                    switch(outcome.DialogResult)
                    {
                        case DialogResult.OK:
                            SaveXmlFile(apiDoc);
                            break;                        
                        default:
                            break;
                    }                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SaveXmlFile(ApiDocumentation apiDoc)
        {
            try
            {
                // Prompt the user to select a path where to save the API XML file
                SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.Filter = "XML files|*.xml";
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    apiDoc.SaveToXML(saveDlg.FileName);
                    Trace.WriteLine("Done.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonBrowseXmlClick(object sender, EventArgs e)
        {
            try
            {
                // Clear any previous errors				
                errorProvider1.Clear();

                OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.Filter = "XML files|*.xml";
                openDlg.CheckFileExists = true;
                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    textBoxDocXml.Text = openDlg.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
