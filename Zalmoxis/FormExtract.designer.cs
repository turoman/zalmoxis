﻿namespace Zalmoxis
{
    partial class FormExtract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBrowseDocXml = new System.Windows.Forms.Button();
            this.textBoxDocXml = new System.Windows.Forms.TextBox();
            this.checkCOM = new System.Windows.Forms.CheckBox();
            this.textBoxSourceDll = new System.Windows.Forms.TextBox();
            this.labelDocXml = new System.Windows.Forms.Label();
            this.labelSourceDll = new System.Windows.Forms.Label();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.btnExtractApiInfo = new System.Windows.Forms.Button();
            this.textBoxViewLog = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(-8, -1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1MinSize = 230;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.textBoxViewLog);
            this.splitContainer1.Panel2MinSize = 250;
            this.splitContainer1.Size = new System.Drawing.Size(532, 363);
            this.splitContainer1.SplitterDistance = 276;
            this.splitContainer1.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Controls.Add(this.btnBrowseDocXml, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxDocXml, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.checkCOM, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxSourceDll, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelDocXml, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelSourceDll, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonBrowse, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnExtractApiInfo, 0, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(263, 166);
            this.tableLayoutPanel1.TabIndex = 41;
            // 
            // btnBrowseDocXml
            // 
            this.btnBrowseDocXml.Location = new System.Drawing.Point(226, 103);
            this.btnBrowseDocXml.MinimumSize = new System.Drawing.Size(25, 19);
            this.btnBrowseDocXml.Name = "btnBrowseDocXml";
            this.btnBrowseDocXml.Size = new System.Drawing.Size(25, 19);
            this.btnBrowseDocXml.TabIndex = 4;
            this.btnBrowseDocXml.Text = "...";
            this.btnBrowseDocXml.UseVisualStyleBackColor = true;
            this.btnBrowseDocXml.Click += new System.EventHandler(this.ButtonBrowseXmlClick);
            // 
            // textBoxDocXml
            // 
            this.textBoxDocXml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDocXml.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxDocXml.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.textBoxDocXml.Location = new System.Drawing.Point(3, 103);
            this.textBoxDocXml.Name = "textBoxDocXml";
            this.textBoxDocXml.Size = new System.Drawing.Size(217, 20);
            this.textBoxDocXml.TabIndex = 3;
            // 
            // checkCOM
            // 
            this.checkCOM.AutoSize = true;
            this.checkCOM.Location = new System.Drawing.Point(3, 53);
            this.checkCOM.Name = "checkCOM";
            this.checkCOM.Size = new System.Drawing.Size(130, 17);
            this.checkCOM.TabIndex = 2;
            this.checkCOM.Text = "Strip the \"Class\" suffix";
            this.checkCOM.UseVisualStyleBackColor = true;
            // 
            // textBoxSourceDll
            // 
            this.textBoxSourceDll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSourceDll.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxSourceDll.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.textBoxSourceDll.Location = new System.Drawing.Point(3, 28);
            this.textBoxSourceDll.Name = "textBoxSourceDll";
            this.textBoxSourceDll.Size = new System.Drawing.Size(217, 20);
            this.textBoxSourceDll.TabIndex = 0;
            // 
            // labelDocXml
            // 
            this.labelDocXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDocXml.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labelDocXml, 2);
            this.labelDocXml.Location = new System.Drawing.Point(3, 87);
            this.labelDocXml.Name = "labelDocXml";
            this.labelDocXml.Size = new System.Drawing.Size(147, 13);
            this.labelDocXml.TabIndex = 38;
            this.labelDocXml.Text = "(Optional) XML comments file:";
            // 
            // labelSourceDll
            // 
            this.labelSourceDll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSourceDll.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labelSourceDll, 2);
            this.labelSourceDll.Location = new System.Drawing.Point(3, 12);
            this.labelSourceDll.Name = "labelSourceDll";
            this.labelSourceDll.Size = new System.Drawing.Size(46, 13);
            this.labelSourceDll.TabIndex = 36;
            this.labelSourceDll.Text = "DLL file:";
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(226, 28);
            this.buttonBrowse.MinimumSize = new System.Drawing.Size(25, 19);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(25, 19);
            this.buttonBrowse.TabIndex = 1;
            this.buttonBrowse.Text = "...";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.ButtonBrowseClick);
            // 
            // btnExtractApiInfo
            // 
            this.btnExtractApiInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExtractApiInfo.Location = new System.Drawing.Point(3, 128);
            this.btnExtractApiInfo.MaximumSize = new System.Drawing.Size(120, 25);
            this.btnExtractApiInfo.MinimumSize = new System.Drawing.Size(120, 25);
            this.btnExtractApiInfo.Name = "btnExtractApiInfo";
            this.btnExtractApiInfo.Size = new System.Drawing.Size(120, 25);
            this.btnExtractApiInfo.TabIndex = 5;
            this.btnExtractApiInfo.Text = "Extract";
            this.btnExtractApiInfo.UseVisualStyleBackColor = true;
            this.btnExtractApiInfo.Click += new System.EventHandler(this.ButtonExtractClick);
            // 
            // textBoxViewLog
            // 
            this.textBoxViewLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxViewLog.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxViewLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxViewLog.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxViewLog.Location = new System.Drawing.Point(3, 3);
            this.textBoxViewLog.Multiline = true;
            this.textBoxViewLog.Name = "textBoxViewLog";
            this.textBoxViewLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxViewLog.Size = new System.Drawing.Size(246, 356);
            this.textBoxViewLog.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FormExtractApiInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 374);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(405, 274);
            this.Name = "FormExtractApiInfo";
            this.Text = "Extract API Info";
            this.Load += new System.EventHandler(this.FormExtractApiInfo_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnExtractApiInfo;
        private System.Windows.Forms.Label labelSourceDll;
        private System.Windows.Forms.Button buttonBrowse;
        internal System.Windows.Forms.TextBox textBoxSourceDll;
        private System.Windows.Forms.TextBox textBoxViewLog;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.CheckBox checkCOM;
        private System.Windows.Forms.Button btnBrowseDocXml;
        internal System.Windows.Forms.TextBox textBoxDocXml;
        private System.Windows.Forms.Label labelDocXml;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}