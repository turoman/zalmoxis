﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zalmoxis
{
    public partial class FormParent : Form
    {
        private int childFormNumber = 0;

        /// <summary>
        /// FormParent constructor.
        /// </summary>
        public FormParent()
        {
            InitializeComponent();
        }

        // File > Exit
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Edit > Cut
        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        // Edit > Copy
        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.ActiveMdiChild != null)
            {
                switch (this.ActiveMdiChild.Tag.ToString())
                {
                    case "extract_api":
                        FormExtract frmExtract = (FormExtract)this.ActiveMdiChild;
                        if (frmExtract.textBoxSourceDll.Focused)
                            Clipboard.SetText(frmExtract.textBoxSourceDll.SelectedText);
                        break;                    
                    default:
                        break;
                }
            }
        }

        // Edit > Paste
        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(ActiveMdiChild != null)
            {
                switch (this.ActiveMdiChild.Tag.ToString())
                {
                    case "extract_api":
                        FormExtract frmExtract = (FormExtract)this.ActiveMdiChild;
                        if (frmExtract.textBoxSourceDll.Focused)
                            PasteText(frmExtract.textBoxSourceDll);
                        break;                    
                    default:
                        break;
                }
            }
        }

        // Paste is not as simple as it sounds...
        private void PasteText(TextBox tBox)
        {
            try
            {
                // get the text before selection
                string tBefore = tBox.Text.Substring(0, tBox.SelectionStart);

                // get the text after selection          
                int tSelectionEnd = tBefore.Length + tBox.SelectionLength;
                string tAfter = tBox.Text.Substring(tSelectionEnd, tBox.TextLength - tBefore.Length - tBox.SelectionLength);

                // paste the text
                tBox.Text = tBefore + Clipboard.GetText() + tAfter;

                // move the caret to the new position after paste
                tBox.SelectionStart = tBefore.Length + Clipboard.GetText().Length;
                tBox.SelectionLength = 0;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        // File > New Task > Reflect DLL
        private void reflectDLLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormExtract();
            childForm.WindowState = FormWindowState.Maximized;
            childForm.Tag = "extract_api";
            childForm.MdiParent = this;
            childForm.Text = "Extract API Info (task " + childFormNumber++ + ")";
            childForm.Show();
        }

        // Help > Contents
        private void contentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Retrieve help from the online wiki
            string helpPath = "https://bitbucket.org/turoman/zalmoxis/wiki/Home";
            System.Diagnostics.Process.Start(helpPath);
        }

        // Help > About
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form abForm = new FormAbout();
            // Use ShowDialog() to be able to center the form on parent
            abForm.ShowDialog();
        }

        // Edit > Select All
        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.ActiveMdiChild != null)
            {
                switch (this.ActiveMdiChild.Tag.ToString())
                {
                    case "extract_api":
                        FormExtract frmExtract = (FormExtract)this.ActiveMdiChild;
                        if (frmExtract.textBoxSourceDll.Focused)
                            frmExtract.textBoxSourceDll.SelectAll();
                        break;                    
                    default:
                        break;
                }
            }

        }

        // On form load
        private void FormParent_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel.Text = "Version " + this.ProductVersion;
        }
    }
}
