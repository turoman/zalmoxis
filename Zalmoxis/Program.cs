﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Zalmoxis
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormParent());

            // Add logging
            DateTime dt = DateTime.Now;
            Trace.Listeners.Add(new TextWriterTraceListener($"{dt.ToString("yyy-MM-dd")}.log"));
            Trace.AutoFlush = true;
        }
    }
}
